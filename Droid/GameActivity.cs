﻿using Android.App;
using Android.Content;
using Android.Gms.Ads;
using Android.OS;
using Android.Preferences;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using TicTacToe.Droid.Persistence;
using TicTacToe.Droid.Views;

namespace TicTacToe.Droid
{
    [Activity(Label = "GameActivity")]
    public class GameActivity : FragmentActivity
    {
        private const string EXTRA_INITIAL_FIGURE = "extra_initial_figure";

        private TableLayout _table;
        private ProgressBar _progress;

        private TileView[] _tiles;
        private GameViewModel viewModel;

        public static Intent GetIntent(Context context, Figure initialFigure = Figure.X)
        {
            var intent = new Intent(context, typeof(GameActivity));
            intent.PutExtra(EXTRA_INITIAL_FIGURE, (int) initialFigure);
            return intent;
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Game);

            var figure = (Figure) Intent.GetIntExtra(EXTRA_INITIAL_FIGURE, (int) Figure.Zero);

            var preferences = PreferenceManager.GetDefaultSharedPreferences(this);

            viewModel = new GameViewModel( 
                new GameResultChecker(),
                new SharedPreferencePersistence(preferences),
                figure);

            _table = FindViewById<TableLayout>(Resource.Id.table);
            _progress = FindViewById<ProgressBar>(Resource.Id.progress);

            _tiles = new TileView[]
            {
                FindViewById<TileView>(Resource.Id.tl),
                FindViewById<TileView>(Resource.Id.tc),
                FindViewById<TileView>(Resource.Id.tr),

                FindViewById<TileView>(Resource.Id.cl),
                FindViewById<TileView>(Resource.Id.cc),
                FindViewById<TileView>(Resource.Id.cr),

                FindViewById<TileView>(Resource.Id.bl),
                FindViewById<TileView>(Resource.Id.bc),
                FindViewById<TileView>(Resource.Id.br)
            };

            foreach (TileView tile in _tiles)
            {
                tile.Click += (sender, args) =>
                {
                    viewModel.OnTilePressed(tile.Tile);
                };
            }

            viewModel.GameLoadingEvents += GameLoadingChanged;

            viewModel.Events += UpdateTile;

            viewModel.ResultEvents += HandleResult;

            var adView = FindViewById<AdView>(Resource.Id.ad_view);
            var adRequest = new AdRequest.Builder().Build();
            adView.LoadAd(adRequest);
        }

        protected override async void OnStart()
        {
            base.OnStart();

            await viewModel.LoadGame();
        }

        protected override void OnStop()
        {
            base.OnStop();

            viewModel.Dispose();
        }

        private void GameLoadingChanged(GameLoadingState loadingState)
        {
            _progress.Visibility = loadingState == GameLoadingState.Loading ? ViewStates.Visible : ViewStates.Gone;
            _table.Visibility = loadingState == GameLoadingState.Done ? ViewStates.Visible : ViewStates.Gone;
        }

        private void UpdateTile(Tile tile, Figure figure)
        {
            var tileView = FindViewByTile(tile);
            tileView.UpdateWithFigure(figure);
        }

        private TileView FindViewByTile(Tile tile)
        {
            foreach (TileView item in _tiles)
            {
                if (item.Tile == tile)
                {
                    return item;
                }
            }
            return null;
        }

        private void HandleResult(Result result)
        {
            foreach (TileView tile in _tiles)
            {
                tile.Enabled = false;
            }

            ResultDialog.CreateAndShow(SupportFragmentManager, result);
        }

        public override void OnBackPressed()
        {
            viewModel.ClearState();

            base.OnBackPressed();
        }
    }
}
