﻿using Android.Content;
using Android.Util;
using Android.Widget;

namespace TicTacToe.Droid.Views
{
    public class TileView : ImageButton
    {
        public Tile Tile { get; private set; }

        public TileView(Context context) :
            base(context)
        {
        }

        public TileView(Context context, IAttributeSet attrs) :
            base(context, attrs)
        {
            Initialize(context, attrs);
        }

        public TileView(Context context, IAttributeSet attrs, int defStyle) :
            base(context, attrs, defStyle)
        {
            Initialize(context, attrs);
        }

        private void Initialize(Context context, IAttributeSet attrs)
        {
            var typedArray = context.Theme.ObtainStyledAttributes(
                attrs, Resource.Styleable.TileView, 0, 0);

            try
            {
                var tileIndex = typedArray.GetInteger(Resource.Styleable.TileView_tile, 0);
                Tile = ReadTileFromIndex(tileIndex);
            }
            finally
            {
                typedArray.Recycle();
            }
        }

        private Tile ReadTileFromIndex(int tileIndex)
        {
            switch (tileIndex)
            {
                case 0:
                    return Tile.TOP_LEFT;
                case 1:
                    return Tile.TOP_CENTER;
                case 2:
                    return Tile.TOP_RIGHT;
                case 3:
                    return Tile.CENTER_LEFT;
                case 4:
                    return Tile.CENTER_CENTER;
                case 5:
                    return Tile.CENTER_RIGHT;
                case 6:
                    return Tile.BOTTOM_LEFT;
                case 7:
                    return Tile.BOTTOM_CENTER;
                case 8:
                    return Tile.BOTTOM_RIGHT;
            }
            throw new System.Exception($"There is no such tile index: {tileIndex}");
        }

        public void UpdateWithFigure(Figure figure)
        {
            if (figure == Figure.Zero)
            {
                SetImageResource(Resource.Drawable.o_mark);
            }

            if (figure == Figure.X)
            {
                SetImageResource(Resource.Drawable.x_mark);
            }

            Enabled = false;
        }
    }
}
