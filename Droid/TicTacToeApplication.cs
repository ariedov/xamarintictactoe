﻿using System;
using Android.App;
using Android.Gms.Ads;

namespace TicTacToe.Droid
{
    [Application(Theme = "@android:style/Theme.Material")]
    public class TicTacToeApplication: Application
    {
        public TicTacToeApplication(IntPtr javaReference, Android.Runtime.JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public override void OnCreate()
        {
            base.OnCreate();

            MobileAds.Initialize(this, "ca-app-pub-3940256099942544~3347511713");
        }
    }
}
