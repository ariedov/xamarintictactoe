﻿using Android.App;
using Android.Widget;
using Android.OS;
using TicTacToe.Droid.Persistence;
using Android.Preferences;
using Android.Views;

namespace TicTacToe.Droid
{
    [Activity(Label = "TicTacToe", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity
    {
        private MainViewModel viewModel;

        private View _chooser;
        private ProgressBar _progress;

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Main);

            _chooser = FindViewById(Resource.Id.chooser);
            _progress = FindViewById<ProgressBar>(Resource.Id.progress);

            var zeros = FindViewById<Button>(Resource.Id.zeros);
            var xes = FindViewById<Button>(Resource.Id.xes);

            zeros.Click += (sender, view) =>
            {
                StartActivity(GameActivity.GetIntent(this, Figure.Zero));
            };

            xes.Click += (sender, view) =>
            {
                StartActivity(GameActivity.GetIntent(this, Figure.X));
            };

            var preference = PreferenceManager.GetDefaultSharedPreferences(this);
            viewModel = new MainViewModel(new SharedPreferencePersistence(preference));

            System.Action<bool> eventListener = null;
            eventListener = (hasEvent) =>
            {
                _progress.Visibility = ViewStates.Gone;
                _chooser.Visibility = ViewStates.Visible;

                if (hasEvent)
                {
                    StartActivity(GameActivity.GetIntent(this));
                }

                viewModel.HasGameStateEvent -= eventListener;
            };
            viewModel.HasGameStateEvent += eventListener;

            await viewModel.Start();
        }
    }
}

