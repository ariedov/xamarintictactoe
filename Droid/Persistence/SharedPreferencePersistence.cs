﻿using System.Collections.Generic;
using System.Json;
using System.Threading.Tasks;
using Android.Content;

using Newtonsoft.Json;

namespace TicTacToe.Droid.Persistence
{
    public class SharedPreferencePersistence : IGamePersistence
    {
        private const string KEY_TILE_MAP = "key_tile_map";
        private const string KEY_CURRENT_FIGURE = "key_current_figure";

        private ISharedPreferences Prefs { get; }

        public SharedPreferencePersistence(ISharedPreferences prefs)
        {
            Prefs = prefs;
        }

        public Task<GameState> ReadGameState()
        {
            return Task.Run(() =>
            {
                var tileMap = new Dictionary<Tile, Figure>();

                var tileMapJson = Prefs.GetString(KEY_TILE_MAP, string.Empty);
                if (tileMapJson == string.Empty) return new GameState();

                var intMap = JsonConvert.DeserializeObject<Dictionary<int, int>>(tileMapJson);
                foreach (int key in intMap.Keys)
                {
                    tileMap.Add((Tile)key, (Figure)intMap[key]);
                }

                return new GameState(tileMap, (Figure) Prefs.GetInt(KEY_CURRENT_FIGURE, 0));
            });
        }

        public Task SaveGameState(GameState gameState)
        {
            return Task.Run(() => {
                var editor = Prefs.Edit();

                var intMap = new Dictionary<int, int>();
                foreach (Tile tile in gameState.TileMap.Keys)
                {
                    intMap.Add((int)tile, (int)gameState.TileMap[tile]);
                }

                editor.PutString(KEY_TILE_MAP, JsonConvert.SerializeObject(intMap));
                editor.PutInt(KEY_CURRENT_FIGURE, (int) gameState.CurrentFigure);
                editor.Commit();
            });
        }

        public Task ClearState()
        {
            return Task.Run(() =>
            {
                Prefs.Edit()
                .Remove(KEY_TILE_MAP)
                .Remove(KEY_CURRENT_FIGURE)
                .Commit();
            });
        }

        public Task<bool> HasGameState()
        {
            return Task.Run(() => {
                return Prefs.Contains(KEY_CURRENT_FIGURE);
            });
        }
    }
}
