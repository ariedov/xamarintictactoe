﻿using Android.App;
using Android.Content;
using Android.OS;

namespace TicTacToe.Droid
{

    // A DialogFragment will retain on screen rotation. 
    // A regular AlertDialog - will not.
    public class ResultDialog: Android.Support.V4.App.DialogFragment
    {
        private const string EXTRA_RESULT = "extra_result";

        public static void CreateAndShow(Android.Support.V4.App.FragmentManager fragmentManager, Result result)
        {
            var args = new Bundle();
            args.PutInt(EXTRA_RESULT, (int) result);

            var fragment = new ResultDialog();
            fragment.Arguments = args;
            fragment.Cancelable = false;
            fragment.Show(fragmentManager, null);
        }

        public override Dialog OnCreateDialog(Bundle savedInstanceState)
        {
            var result = (Result) Arguments.GetInt(EXTRA_RESULT);

            var dialog = new AlertDialog.Builder(Activity)
                .SetTitle(GetTitleRes(result))
                .SetNeutralButton(Resource.String.okay, (sender, view) => {
                    Activity.Finish();
                })
                .SetPositiveButton(Resource.String.share, (sender, view) =>
                {
                    Activity.Finish();
                    ShareVictory();
                })
                .SetCancelable(false)
                .Create();

            return dialog;
        }

        private int GetTitleRes(Result result)
        {
            switch (result)
            {
                case Result.XWin:
                    return Resource.String.x_wins;
                case Result.ZeroWin:
                    return Resource.String.o_wins;                
                case Result.Draw:
                    return Resource.String.draw;
            }

            return 0;
        }

        private void ShareVictory()
        {
            var shareIntent = new Intent();
            shareIntent.SetAction(Intent.ActionSend);
            shareIntent.SetType("text/plain");
            shareIntent.PutExtra(Intent.ExtraText, "I've just won a tick-tack-toe game!");
            Activity.StartActivity(Intent.CreateChooser(shareIntent, string.Empty));
        }
    }
}
