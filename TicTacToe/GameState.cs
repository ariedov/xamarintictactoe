﻿using System;
using System.Collections.Generic;

namespace TicTacToe
{
    public struct GameState
    {
        public Dictionary<Tile, Figure> TileMap { get; }
        public Figure CurrentFigure { get; }

        public GameState(Dictionary<Tile, Figure> tileMap, Figure currentFigure) : this()
        {
            TileMap = tileMap;
            CurrentFigure = currentFigure;
        }
    }
}
