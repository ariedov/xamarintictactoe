﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TicTacToe
{
    public interface IGamePersistence
    {
        Task SaveGameState(GameState gameState);

        Task ClearState();

        Task<GameState> ReadGameState();

        Task<bool> HasGameState();
    }
}
