﻿using System.Collections.Generic;

namespace TicTacToe
{
    public interface IGameResultChecker
    {
        bool IsFigureWins(Dictionary<Tile, Figure> tileMap, Figure figure);

        bool IsDraw(Dictionary<Tile, Figure> tileMap);
    }

    public class GameResultChecker : IGameResultChecker
    {
        public bool IsFigureWins(Dictionary<Tile, Figure> tileMap, Figure figure)
        {
            return IsTopMatches(tileMap, figure)
                || IsLeftMatches(tileMap, figure)
                || IsRightMatches(tileMap, figure)
                || IsBottomMatches(tileMap, figure)
                || IsVerticalCenterMatches(tileMap, figure)
                || IsHorizontalCenterMatches(tileMap, figure)
                || IsLeftDiagonalMatches(tileMap, figure)
                || IsRightDiagonalMatches(tileMap, figure);
        }

        private bool IsTopMatches(Dictionary<Tile, Figure> tileMap, Figure figure)
        {
            return
                 tileMap.ContainsKey(Tile.TOP_LEFT)
                 && tileMap.ContainsKey(Tile.TOP_CENTER)
                 && tileMap.ContainsKey(Tile.TOP_RIGHT)

                 && tileMap[Tile.TOP_LEFT] == figure
                 && tileMap[Tile.TOP_CENTER] == figure
                 && tileMap[Tile.TOP_RIGHT] == figure;
        }

        private bool IsLeftMatches(Dictionary<Tile, Figure> tileMap, Figure figure)
        {
            return tileMap.ContainsKey(Tile.TOP_LEFT)
                 && tileMap.ContainsKey(Tile.CENTER_LEFT)
                 && tileMap.ContainsKey(Tile.BOTTOM_LEFT)

                 && tileMap[Tile.TOP_LEFT] == figure
                 && tileMap[Tile.CENTER_LEFT] == figure
                 && tileMap[Tile.BOTTOM_LEFT] == figure;
        }

        private bool IsRightMatches(Dictionary<Tile, Figure> tileMap, Figure figure)
        {
            return tileMap.ContainsKey(Tile.TOP_RIGHT)
                 && tileMap.ContainsKey(Tile.CENTER_RIGHT)
                 && tileMap.ContainsKey(Tile.BOTTOM_RIGHT)

                 && tileMap[Tile.TOP_RIGHT] == figure
                 && tileMap[Tile.CENTER_RIGHT] == figure
                 && tileMap[Tile.BOTTOM_RIGHT] == figure;
        }

        private bool IsHorizontalCenterMatches(Dictionary<Tile, Figure> tileMap, Figure figure)
        {
            return tileMap.ContainsKey(Tile.CENTER_LEFT)
                 && tileMap.ContainsKey(Tile.CENTER_RIGHT)
                 && tileMap.ContainsKey(Tile.CENTER_CENTER)

                 && tileMap[Tile.CENTER_LEFT] == figure
                 && tileMap[Tile.CENTER_RIGHT] == figure
                 && tileMap[Tile.CENTER_CENTER] == figure;
        }

        private bool IsVerticalCenterMatches(Dictionary<Tile, Figure> tileMap, Figure figure)
        {
            return tileMap.ContainsKey(Tile.TOP_CENTER)
                 && tileMap.ContainsKey(Tile.CENTER_CENTER)
                 && tileMap.ContainsKey(Tile.BOTTOM_CENTER)

                 && tileMap[Tile.TOP_CENTER] == figure
                 && tileMap[Tile.CENTER_CENTER] == figure
                 && tileMap[Tile.BOTTOM_CENTER] == figure;
        }

        private bool IsBottomMatches(Dictionary<Tile, Figure> tileMap, Figure figure)
        {
            return tileMap.ContainsKey(Tile.BOTTOM_LEFT)
                 && tileMap.ContainsKey(Tile.BOTTOM_CENTER)
                 && tileMap.ContainsKey(Tile.BOTTOM_RIGHT)

                 && tileMap[Tile.BOTTOM_LEFT] == figure
                 && tileMap[Tile.BOTTOM_CENTER] == figure
                 && tileMap[Tile.BOTTOM_RIGHT] == figure;
        }

        private bool IsLeftDiagonalMatches(Dictionary<Tile, Figure> tileMap, Figure figure)
        {
            return tileMap.ContainsKey(Tile.TOP_LEFT)
                 && tileMap.ContainsKey(Tile.CENTER_CENTER)
                 && tileMap.ContainsKey(Tile.BOTTOM_RIGHT)

                 && tileMap[Tile.TOP_LEFT] == figure
                 && tileMap[Tile.CENTER_CENTER] == figure
                 && tileMap[Tile.BOTTOM_RIGHT] == figure;
        }

        private bool IsRightDiagonalMatches(Dictionary<Tile, Figure> tileMap, Figure figure)
        {
            return tileMap.ContainsKey(Tile.TOP_RIGHT)
                 && tileMap.ContainsKey(Tile.CENTER_CENTER)
                 && tileMap.ContainsKey(Tile.BOTTOM_LEFT)

                 && tileMap[Tile.TOP_RIGHT] == figure
                 && tileMap[Tile.CENTER_CENTER] == figure
                 && tileMap[Tile.BOTTOM_LEFT] == figure;
        }

        public bool IsDraw(Dictionary<Tile, Figure> tileMap)
        {
            return !IsFigureWins(tileMap, Figure.X) 
                    && !IsFigureWins(tileMap, Figure.Zero) 

                    && tileMap.ContainsKey(Tile.TOP_LEFT)
                    && tileMap.ContainsKey(Tile.TOP_CENTER)
                    && tileMap.ContainsKey(Tile.TOP_RIGHT)

                    && tileMap.ContainsKey(Tile.CENTER_LEFT)
                    && tileMap.ContainsKey(Tile.CENTER_CENTER)
                    && tileMap.ContainsKey(Tile.CENTER_RIGHT)

                    && tileMap.ContainsKey(Tile.BOTTOM_LEFT)
                    && tileMap.ContainsKey(Tile.BOTTOM_CENTER)
                    && tileMap.ContainsKey(Tile.BOTTOM_RIGHT);
        }
    }
}
