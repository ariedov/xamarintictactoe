﻿using System;
using System.Threading.Tasks;

namespace TicTacToe
{
    public class MainViewModel
    {
        public event Action<bool> HasGameStateEvent;

        private IGamePersistence GamePersistence { get; }

        public MainViewModel(IGamePersistence gamePersistence)
        {
            GamePersistence = gamePersistence;
        }

        public async Task Start()
        {
            var hasGameState = await GamePersistence.HasGameState();
            HasGameStateEvent?.Invoke(hasGameState);
        }
    }
}
