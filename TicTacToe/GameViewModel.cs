﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace TicTacToe
{
    public class GameViewModel
    {
        public event Action<Tile, Figure> Events;
        public event Action<Result> ResultEvents;
        public event Action<GameLoadingState> GameLoadingEvents;

        private IGameResultChecker _resultChecker;
        private IGamePersistence _gamePersistence;

        private Dictionary<Tile, Figure> _tileMap = new Dictionary<Tile, Figure>();
        private Figure _currentFigure;

        private CancellationTokenSource _cancellationTokenSource;

        public GameViewModel(
            IGameResultChecker resultChecker,
            IGamePersistence gamePersistence,
            Figure initialFigure)
        {
            _resultChecker = resultChecker;
            _gamePersistence = gamePersistence;

            _currentFigure = initialFigure;
        }

        public async Task LoadGame()
        {
            GameLoadingEvents?.Invoke(GameLoadingState.Loading);

            _cancellationTokenSource = new CancellationTokenSource();
            await Task.Run(() => 
            {
                var gameState = _gamePersistence.ReadGameState().Result;
                if (gameState.TileMap != null)
                {
                    _currentFigure = gameState.CurrentFigure;
                    _tileMap = gameState.TileMap;
                    foreach (Tile tile in _tileMap.Keys)
                    {
                        Events?.Invoke(tile, _tileMap[tile]);
                    }
                }
            }, _cancellationTokenSource.Token);
            GameLoadingEvents?.Invoke(GameLoadingState.Done);
        }

        public void Dispose()
        {
            _cancellationTokenSource.Dispose();
        }

        public void OnTilePressed(Tile tile)
        {
            // prefer to return early
            if (_tileMap.ContainsKey(tile)) return;

            _tileMap.Add(tile, _currentFigure);
            Events?.Invoke(tile, _currentFigure);

            ResetCurrentFigure();

            _gamePersistence.SaveGameState(new GameState(_tileMap, _currentFigure));

            CheckScore();
        }

        private void ResetCurrentFigure()
        {
            if (_currentFigure == Figure.Zero)
            {
                _currentFigure = Figure.X;
            }
            else
            {
                _currentFigure = Figure.Zero;
            }
        }

        private void CheckScore()
        {
            if (_resultChecker.IsDraw(_tileMap))
            {
                ClearState();
                ResultEvents?.Invoke(Result.Draw);
            }

            if (IsZeroWin())
            {
                ClearState();
                ResultEvents?.Invoke(Result.ZeroWin);
            }

            if (IsXWin())
            {
                ClearState();
                ResultEvents?.Invoke(Result.XWin);
            }
        }

        public async void ClearState()
        {
            await _gamePersistence.ClearState();
        }

        private bool IsZeroWin()
        {
            return _resultChecker.IsFigureWins(_tileMap, Figure.Zero);
        }

        private bool IsXWin()
        {
            return _resultChecker.IsFigureWins(_tileMap, Figure.X);
        }
    }

    public enum Tile
    {
        TOP_LEFT, TOP_CENTER, TOP_RIGHT,
        CENTER_LEFT, CENTER_CENTER, CENTER_RIGHT,
        BOTTOM_LEFT, BOTTOM_CENTER, BOTTOM_RIGHT
    }

    public enum Figure {
        Zero = 0, 
        X = 1,
    }

    public enum GameLoadingState {
        Loading, Done
    }

    public enum Result
    {
        ZeroWin, XWin, Draw
    }
}
